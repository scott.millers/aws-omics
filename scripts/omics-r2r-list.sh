#!/bin/bash
# List the OMICS workflows
# https://docs.aws.amazon.com/omics/latest/dev/service-workflows-ci.html

#aws omics list-workflows --type READY2RUN
aws omics get-workflow --type READY2RUN --id 4414139