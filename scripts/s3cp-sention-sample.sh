#!/bin/bash
# Script to copy Sentieon FASTQ WGS for up to 32x sample data to my s3 bucket
# https://us-east-1.console.aws.amazon.com/omics/home?region=us-east-1#/ready2run/4414139

aws s3 cp s3://omics-us-east-1/sample-inputs/4414139/HG002.novaseq.pcr-free.32x.R1.fastq.gz s3://804609861260-bioinformatics-infectious-disease/Omics/sample-input-SentieonGermlineFASTQWGS32x/
aws s3 cp s3://omics-us-east-1/sample-inputs/4414139/HG002.novaseq.pcr-free.32x.R2.fastq.gz s3://804609861260-bioinformatics-infectious-disease/Omics/sample-input-SentieonGermlineFASTQWGS32x/