#!/bin/bash
# Run my workflows
# https://docs.aws.amazon.com/omics/latest/dev/service-workflows-ci.html


#PARAM=file:///home/scott/environment/repos/aws-omics/scripts/parameters/Sentieon-4414139.json

ROLE="OmicsR2RWorkflowRole"
PARAM="file:///home/ec2-user/environment/repos/aws-omics/scripts/parameters/sentieon-sample.json"
#PARAM="file:///home/scott/environment/repos/aws-omics/scripts/parameters/sentieon-sample.json"

aws omics start-run \
  --debug \
  --workflow-type READY2RUN \
  --workflow-id 4414139 \
  --output-uri https://804609861260-bioinformatics-infectious-disease.s3.amazonaws.com/Omics/output-sample-4414139/ \
  --role-arn $ROLE \
  --parameters $PARAM
