#!/bin/bash
# Script to copy Sentieon FASTQ WGS for up to 32x sample data to my s3 bucket
# https://us-east-1.console.aws.amazon.com/omics/home?region=us-east-1#/ready2run/4414139


#LDIR=/mnt/c/Users/SMiller02/Downloads/
LDIR=C:\Users\SMiller02\Downloads\


# Get the new files from AWS Gov
# These don't work.  I don't have access through the CLI
aws s3 cp s3://804609861260-bioinformatics-nbs/SampleFiles/230520/WGS20230001-TX-VH00729-230520_S1_R1_001.fastq.gz --profile bio-prod
aws s3 cp s3://804609861260-bioinformatics-nbs/SampleFiles/230520/WGS20230001-TX-VH00729-230520_S1_R2_001.fastq.gz --profile bio-prod

aws s3 cp s3://804609861260-bioinformatics-nbs/SampleFiles/230520/WGS20230003-TX-VH00729-230520_S2_R1_001.fastq.gz . --profile bio-prod
aws s3 cp s3://804609861260-bioinformatics-nbs/SampleFiles/230520/WGS20230003-TX-VH00729-230520_S2_R2_001.fastq.gz . --profile bio-prod

aws
aws s3 cp s3://804609861260-bioinformatics-nbs/SampleFiles/230520/WGS20230009-TX-VH00729-230520_S3_R2_001.fastq.gz --profile bio-prod


# Copy to sandbox account
# First traunch
aws s3 cp WGS20230001-TX-VH00729-230520_S1_R1_001.fastq.gz s3://804609861260-bioinformatics-infectious-disease/Omics/input-230520/
aws s3 cp WGS20230001-TX-VH00729-230520_S1_R2_001.fastq.gz s3://804609861260-bioinformatics-infectious-disease/Omics/input-230520/

# Second traunch
aws s3 cp WGS20230003-TX-VH00729-230520_S2_R1_001.fastq.gz s3://804609861260-bioinformatics-infectious-disease/Omics/input-230520/
aws s3 cp WGS20230003-TX-VH00729-230520_S2_R2_001.fastq.gz s3://804609861260-bioinformatics-infectious-disease/Omics/input-230520/

# Third traunch
aws s3 cp WGS20230009-TX-VH00729-230520_S3_R1_001.fastq.gz s3://804609861260-bioinformatics-infectious-disease/Omics/input-230520/
aws s3 cp WGS20230009-TX-VH00729-230520_S3_R2_001.fastq.gz s3://804609861260-bioinformatics-infectious-disease/Omics/input-230520/