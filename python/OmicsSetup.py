from datetime import datetime
import itertools as it
import json
import gzip
import os
from pprint import pprint
import time
import urllib
import boto3
import botocore.exceptions
import requests

demo_policy = {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "omics:*"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ram:AcceptResourceShareInvitation",
        "ram:GetResourceShareInvitations"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetBucketLocation",
        "s3:PutObject",
        "s3:GetObject",
        "s3:ListBucket",
        "s3:AbortMultipartUpload",
        "s3:ListMultipartUploadParts",
        "s3:GetObjectAcl",
        "s3:PutObjectAcl"
      ],
      "Resource": "*"
    }
  ]
}

demo_trust_policy = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "omics.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}





def create_omics_role():
    # set a timestamp
    #dt_fmt = '%Y%m%dT%H%M%S'
    #ts = datetime.now().strftime(dt_fmt)
    
    # We will use this as the base name for our role and policy
    #omics_iam_name = f'OmicsTutorialRole-{ts}'
    
    omics_iam_name = 'OmicsTutorialRole'
    
    #print('Buckets:\n\t', *[b.name for b in s3_resource.buckets.all()], sep="\n\t")
    print('Role Name:', omics_iam_name)
    
    # Create the iam client
    iam = boto3.resource('iam')
    
   # Check if the role already exist if not create it
    try:
        role = iam.Role(omics_iam_name)
        role.load()
    except botocore.exceptions.ClientError as ex:
    
        if ex.response["Error"]["Code"] == "NoSuchEntity":
            #Create the role with the corresponding trust policy
            role = iam.create_role(
                RoleName=omics_iam_name, 
                AssumeRolePolicyDocument=json.dumps(demo_trust_policy))
            
            #Create policy
            policy = iam.create_policy(
                PolicyName='{}-policy'.format(omics_iam_name), 
                Description="Policy for Amazon Omics demo",
                PolicyDocument=json.dumps(demo_policy))
            
            #Attach the policy to the role
            policy.attach_role(RoleName=omics_iam_name)
    else:
        print('Somthing went wrong, please retry and check your account settings and permissions')
        
        
def get_role_arn(role_name):
    try:
            iam = boto3.resource('iam')
            role = iam.Role(role_name)
            role.load()  # calls GetRole to load attributes
            print("Congratulations, you role named %s is setup!"%role_name)
    except botocore.exeptions.ClientError:
            print("Couldn't get role named %s."%role_name)
            raise
    else:
            return role.arn       
        


def main():
   
   
    try:
        
        omics_iam_name = 'OmicsTutorialRole'
         
         
         # create a omics client
        client = boto3.client('omics', region_name='us-east-1')
        
        # create the role
        create_omics_role()
        #get_role_arn(omics_iam_name)


        
    except botocore.exceptions.ClientError as e :
        print('Error in Main:', e)


if __name__ == '__main__':
    main()
