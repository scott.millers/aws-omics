import sys
import boto3
from botocore.exceptions import ClientError


def list_my_buckets(s3_resource):
    print('Buckets:\n\t', *[b.name for b in s3_resource.buckets.all()], sep="\n\t")


def create_and_delete_my_bucket(s3_resource, bucket_name, keep_bucket):
    list_my_buckets(s3_resource)

    try:
        print('\nCreating new bucket:', bucket_name)
        bucket = s3_resource.create_bucket(
            Bucket=bucket_name,
            CreateBucketConfiguration={
                'LocationConstraint': s3_resource.meta.client.meta.region_name
            }
        )
    except ClientError as e:
        print(f"Couldn't create a bucket for the demo. Here's why: "
              f"{e.response['Error']['Message']}")
        raise

    bucket.wait_until_exists()
    list_my_buckets(s3_resource)

    if not keep_bucket:
        print('\nDeleting bucket:', bucket.name)
        bucket.delete()

        bucket.wait_until_not_exists()
        list_my_buckets(s3_resource)
    else:
        print('\nKeeping bucket:', bucket.name)


def main():
    
    region = "us-east-1"   
    s3_resource = (
        boto3.resource('s3', region_name=region) if region
        else boto3.resource('s3'))
    try:
        list_my_buckets(s3_resource)
    except ClientError:
        print('Exiting the demo.')


if __name__ == '__main__':
    main()
