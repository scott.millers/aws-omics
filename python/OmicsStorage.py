from datetime import datetime
import itertools as it
import json
import gzip
import os
from pprint import pprint
import time
import urllib

import boto3
import botocore.exceptions
import requests

demo_policy = {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "omics:*"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ram:AcceptResourceShareInvitation",
        "ram:GetResourceShareInvitations"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetBucketLocation",
        "s3:PutObject",
        "s3:GetObject",
        "s3:ListBucket",
        "s3:AbortMultipartUpload",
        "s3:ListMultipartUploadParts",
        "s3:GetObjectAcl",
        "s3:PutObjectAcl"
      ],
      "Resource": "*"
    }
  ]
}

demo_trust_policy = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "omics.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}


SOURCE_S3_URI_TPL = {
    "reads": {
        "fastq": "s3://giab/data/NA12878/NIST_NA12878_HG001_HiSeq_300x/140407_D00360_0016_AH948VADXX/Project_RM8398/Sample_U0a/U0a_CGATGT_L001_R{read}_{part:03}.fastq.gz",
    }
}
SOURCE_S3_URIS = {
    "reference": "s3://1000genomes-dragen-v3.7.6/references/fasta/hg38.fa",
    "reads": {
        "fastq": [
            SOURCE_S3_URI_TPL['reads']['fastq'].format(read=read, part=part) 
            for read, part, in list(it.product([1,2], [1,2,3,4]))],
        "cram": [
            "s3://1000genomes/1000G_2504_high_coverage/additional_698_related/data/ERR3988761/HG00405.final.cram",
            "s3://1000genomes/1000G_2504_high_coverage/additional_698_related/data/ERR3988762/HG00408.final.cram",
            "s3://1000genomes/1000G_2504_high_coverage/additional_698_related/data/ERR3988763/HG00418.final.cram",
            "s3://1000genomes/1000G_2504_high_coverage/additional_698_related/data/ERR3988764/HG00420.final.cram",
        ]
    }
}

omics_iam_name = 'OmicsTutorialRole'


def create_omics_role():
    # set a timestamp
    #dt_fmt = '%Y%m%dT%H%M%S'
    #ts = datetime.now().strftime(dt_fmt)
    
    # We will use this as the base name for our role and policy
    #omics_iam_name = f'OmicsTutorialRole-{ts}'
    
    omics_iam_name = 'OmicsTutorialRole'
    
    #print('Buckets:\n\t', *[b.name for b in s3_resource.buckets.all()], sep="\n\t")
    print('Role Name:', omics_iam_name)
    
    # Create the iam client
    iam = boto3.resource('iam')
    
   # Check if the role already exist if not create it
    try:
        role = iam.Role(omics_iam_name)
        role.load()
    except botocore.exceptions.ClientError as ex:
    
        if ex.response["Error"]["Code"] == "NoSuchEntity":
            #Create the role with the corresponding trust policy
            role = iam.create_role(
                RoleName=omics_iam_name, 
                AssumeRolePolicyDocument=json.dumps(demo_trust_policy))
            
            #Create policy
            policy = iam.create_policy(
                PolicyName='{}-policy'.format(omics_iam_name), 
                Description="Policy for Amazon Omics demo",
                PolicyDocument=json.dumps(demo_policy))
            
            #Attach the policy to the role
            policy.attach_role(RoleName=omics_iam_name)
    else:
        print('Somthing went wrong, please retry and check your account settings and permissions')
        
        
def get_role_arn(role_name):
    try:
            iam = boto3.resource('iam')
            role = iam.Role(role_name)
            role.load()  # calls GetRole to load attributes
            print("Congratulations, you role named %s is setup!"%role_name)
    except botocore.exeptions.ClientError:
            print("Couldn't get role named %s."%role_name)
            raise
    else:
            return role.arn       
        
def get_ref_store_id(omics=None):
    if not omics:
        omics = boto3.client('omics')
    
    resp = omics.list_reference_stores(maxResults=10)
    list_of_stores = resp.get('referenceStores')
    store_id = None
    
    if list_of_stores != None:
        # As mentioned above there can only be one store per region
        # if there is a store present is the first one
        store_id = list_of_stores[0].get('id')
    
    return store_id 
    
def setup_ref_store(omics=None):
    
    print(f"Checking for a reference store in region: {omics.meta.region_name}")
    if get_ref_store_id(omics) == None:
        response = omics.create_reference_store(name='myReferenceStore')
        print(response)
    else:
        print("Congratulations, you have an existing reference store named myReferenceStore!")



def import_ref(omics=None):
    ref_name = f'tutorial-1kg-grch38'

    ref_import_job = omics.start_reference_import_job(
        referenceStoreId=get_ref_store_id(omics), 
        roleArn=get_role_arn(omics_iam_name),
        sources=[{
            'sourceFile': SOURCE_S3_URIS["reference"],
            'name': ref_name,
            'tags': {'SourceLocation': '1kg'}
    }])



def main():
   
   
    try:
        
         
         
         # create a omics client
        client = boto3.client('omics', region_name='us-east-1')
        
        # create the role
        create_omics_role()
        #get_role_arn(omics_iam_name)

        # create a reference store
        setup_ref_store(client)
        
        # import references
        import_ref(client)
        
    except botocore.exceptions.ClientError as e :
        print('Error in Main:', e)


if __name__ == '__main__':
    main()
